import flask

# Initialise Flask
app = flask.Flask(__name__)

app.config.from_pyfile('config.py')