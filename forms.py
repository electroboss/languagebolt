from wtforms import StringField, EmailField, PasswordField, SubmitField, SelectField
from wtforms.validators import Length, DataRequired, EqualTo, Email
from flask_wtf import FlaskForm
from models import GAMES, GAMETYPES

class SignupForm(FlaskForm):
  username = StringField("Username", validators=[DataRequired("Please input a username."),Length(max=80)])
  email = EmailField("Email", validators=[
    Email("Please input a valid email"),
    Length(max=120),
    DataRequired("Please input a valid email")
    ])
  password = PasswordField("Password", validators=[
    DataRequired("Please input a password"),
    EqualTo("confirm", message="Passwords must match")
    ])
  confirm = PasswordField("Repeat password")
  submit = SubmitField("Submit")

class LoginForm(FlaskForm):
  login_name = StringField("Username", validators=[DataRequired("Please input a username or email."),Length(max=120)])
  password = PasswordField("Password", validators=[
    DataRequired("Please input a password"),
    ])
  submit = SubmitField("Submit")

class NonValidatingSelectField(SelectField):
  def pre_validate(self, form):
    pass

class GameSelectForm(FlaskForm):
  game_type = SelectField("Game type", choices=list(enumerate(GAMETYPES)), validators=[DataRequired("Please input a gametype")])
  game = SelectField("Game", choices=list(enumerate(GAMES)), validators=[DataRequired("Please input a game")])
  wordlist = NonValidatingSelectField("Word list")
  submit = SubmitField("Submit")