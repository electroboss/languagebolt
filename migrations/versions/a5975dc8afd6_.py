"""empty message

Revision ID: a5975dc8afd6
Revises: 
Create Date: 2022-10-13 19:59:16.447871

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a5975dc8afd6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('student_class',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=80), nullable=False),
    sa.Column('assignments', sa.PickleType(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('username', sa.String(length=80), nullable=False),
    sa.Column('email', sa.String(length=120), nullable=False),
    sa.Column('password_hash', sa.String(length=64), nullable=False),
    sa.Column('isTeacher', sa.Boolean(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('username')
    )
    op.create_table('student',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('scores', sa.PickleType(), nullable=True),
    sa.Column('studentclass_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['studentclass_id'], ['student_class.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('teacher',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('teacher_to_class_association',
    sa.Column('teacher_id', sa.Integer(), nullable=False),
    sa.Column('student_class_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['student_class_id'], ['student_class.id'], ),
    sa.ForeignKeyConstraint(['teacher_id'], ['teacher.id'], ),
    sa.PrimaryKeyConstraint('teacher_id', 'student_class_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('teacher_to_class_association')
    op.drop_table('teacher')
    op.drop_table('student')
    op.drop_table('user')
    op.drop_table('student_class')
    # ### end Alembic commands ###
