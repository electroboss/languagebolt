from flask_sqlalchemy import SQLAlchemy, orm
from flask_migrate import Migrate
from common import app
from copy import deepcopy as dc

# Flask SQL
db = SQLAlchemy(app)
migrate = Migrate(app,db)

# User classes
# Assocation table for teacher-class many-many database orm.relationships
teacher_to_class_association_table = db.Table(
  "teacher_to_class_association",
  db.Model.metadata,
  db.Column("teacher_id", db.ForeignKey("teacher.id"), primary_key=True),
  db.Column("student_class_id", db.ForeignKey("student_class.id"), primary_key=True)
)
# Class class
class StudentClassDB(db.Model):
  __tablename__ = "student_class"
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(80), unique=False, nullable=False)
  assignments = db.Column(db.PickleType(), default=[])
  teachers = orm.relationship(
    "TeacherDB", secondary=teacher_to_class_association_table, backref="studentclasses"
  )
  students = orm.relationship(
    "StudentDB", back_populates="studentclass"
  )

# Student class
class UserDB(db.Model):
  __tablename__ = "user"
  id = db.Column(db.Integer, primary_key=True)

  username = db.Column(db.String(80), unique=True, nullable=False)
  email = db.Column(db.String(120), unique=True, nullable=False)
  password_hash = db.Column(db.String(64), nullable=False)
  isTeacher = db.Column(db.Boolean(), nullable=False)
  student = orm.relationship("StudentDB", back_populates="user", uselist=False)
  teacher = orm.relationship("TeacherDB", back_populates="user", uselist=False)

  def __repr__(self):
    return '<User %r>' % self.username

class StudentDB(db.Model):
  __tablename__ = "student"
  id = db.Column(db.Integer, primary_key=True)
  user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
  user = orm.relationship("UserDB", back_populates="student")

  scores = db.Column(db.PickleType(), default={})
  studentclass_id = db.Column(db.Integer, db.ForeignKey("student_class.id"))
  studentclass = orm.relationship("StudentClassDB", back_populates="students")

class TeacherDB(db.Model):
  __tablename__ = "teacher"
  id = db.Column(db.Integer, primary_key=True)
  user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
  user = orm.relationship("UserDB", back_populates="teacher")

def getClass(teacher: TeacherDB, classname: str):
    return [cl.id for cl in teacher.studentclasses if cl.name == classname][0]

def gameConvert(assignment: [int, int, int]) -> [str, str, int]:
  assignment = dc(assignment)
  if assignment[0] == 0:
    assignment[0] = "Reading"
  else:
    assignment[0] = "Invalid"
  if assignment[1] == 0:
    assignment[1] = "Multiple choice"
  else:
    assignment[1] = "Invalid"
  return assignment

def getScore(scores, assignment) -> int:
  if str(assignment[0])+","+str(assignment[1])+","+str(assignment[2]) not in scores:
    out = 0
  else:
    out = scores[str(assignment[0])+","+str(assignment[1])+","+str(assignment[2])]
  return out

GAMETYPES = [
  "Multiple Choice"
]
GAMES = [
  "Reading"
]