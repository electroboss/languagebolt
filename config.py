import os

SECRET_KEY = str(os.getenv("APP_SECRET_KEY"))
SQLALCHEMY_DATABASE_URI = ("postgresql" + str(os.getenv("DATABASE_URL"))[8:]) if str(os.getenv("DATABASE_URL"))[:8]=="postgres" else str(os.getenv("DATABASE_URL"))
SQLALCHEMY_TRACK_MODIFICATIONS = bool(os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS"))
STATIC = str(os.getenv("STATIC_URL"))
