import flask, flask_login, flask_wtf
from flask_hashing import Hashing
from flask_mobility import Mobility

from common import *
from models import *
from forms import *

# Flask Login
login_manager = flask_login.LoginManager()
login_manager.init_app(app)

# Flask hashing
hashing = Hashing(app)

# Flask Mobility
mobility = Mobility(app)

class User(flask_login.UserMixin):
  def __init__(self,id,username,email,isTeacher):
    super().__init__()
    self.id = id
    self.username = username
    self.email = email
    self.isTeacher = isTeacher


# User stuff
@login_manager.user_loader
def user_loader(id):
  if UserDB.query.get(id) == None:
    return

  user = User(id=id,username=UserDB.query.get(id).username,email=UserDB.query.get(id).email, isTeacher=UserDB.query.get(id).isTeacher)
  return user

@login_manager.request_loader
def request_loader(request):
  id = request.form.get('id')
  if UserDB.query.get(id) == None:
    return

  user = User(id=id,username=UserDB.query.get(id).username,email=UserDB.query.get(id).email, isTeacher=UserDB.query.get(id).isTeacher)
  return user

# Index page
@app.route("/")
def index():
  user = flask_login.current_user
  if not user.is_authenticated:
    return flask.render_template('index/index-nologin.html')
  if not user.isTeacher:
    return flask.render_template('index/index-student.html')
  if user.isTeacher:
    return flask.render_template('index/index-teacher.html')
  return "Aaaagh! WHAT! HOW?!?!??!??", 500

# Signup page
@app.route("/signup-teacher",methods=['GET', 'POST'])
def signup():
  form = SignupForm()
  
  if flask.request.method == "POST":
    if form.validate() == False:
      flask.flash("All fields are required")
      return flask.render_template('accounts/signup.html',form=form)
    username = flask.request.form["username"]
    email = flask.request.form["email"]
    password = flask.request.form["password"]
    confirm_password = flask.request.form["confirm"]
    if UserDB.query.filter_by(username=username).first() == None and UserDB.query.filter_by(email=email).first() == None and confirm_password==password and password != "":
      # TODO: Check email.
      new_user = UserDB(username=username, password_hash=hashing.hash_value(password), email=email, isTeacher=True)
      new_teacher = TeacherDB(user=new_user)
      db.session.add(new_user)
      db.session.add(new_teacher)
      db.session.commit()
      return flask.redirect(flask.url_for("login"))
    
    flask.flash("Username or Email already taken.")
    return flask.render_template('accounts/signup.html',form=form)
  
  if flask.request.method == "GET":
    return flask.render_template('accounts/signup.html',form=form)

  return 'Alright, own up: Who messed up the signup process?'

# Login page
@app.route("/login",methods=['GET', 'POST'])
def login():
  form = LoginForm()
  if flask.request.method == "GET":
    return flask.render_template('accounts/login.html', form=form)

  if flask.request.method == "POST":
    if form.validate() == False:
      flask.flash("All fields are required.")
      return flask.render_template("accounts/login.html", form=form)
    else:
      login_name = flask.request.form['login_name']
      password = flask.request.form['password']
      user = UserDB.query.filter_by(username=login_name).first()
      if user != None:
        if hashing.hash_value(password) == user.password_hash:
          user = User(id=user.id, username=login_name, email=user.email, isTeacher=user.isTeacher)
          flask_login.login_user(user)
          return flask.redirect(flask.url_for("index"))
      user = UserDB.query.filter_by(email=login_name).first()
      if user != None:
        if hashing.hash_value(password) == user.password_hash:
          user = User(id=user.id, email=login_name, username=user.username, isTeacher=user.isTeacher)
          flask_login.login_user(user)
          return flask.redirect(flask.url_for("index"))
        
      flask.flash("Incorrect username or password.")
      return flask.render_template('accounts/login.html', form=form)

  return "Um. Hm. Ah. Errm. What?"

# Logout page
@app.route("/logout")
@flask_login.login_required
def logout():
  flask_login.logout_user()
  return flask.redirect(flask.url_for("index"))

# Unauthorised handler
@login_manager.unauthorized_handler
def unauthorized_handler():
  return flask.abort(401)

@app.route("/game",methods=['GET', 'POST'])
@flask_login.login_required
def game():
  if flask_login.current_user.isTeacher:
    return "This isn't available to teachers. Go plan a lesson or something. Mark books. Stuff. Finally find a working whiteboard pen. Be frustrated at SIMS."
  if flask.request.method == "GET":
    if flask.request.args.get("done") == "1":
      return flask.render_template(
        "game-done.html"
      )
    if flask.request.args.get("gametype") == None or flask.request.args.get("game") == None or flask.request.args.get("wordlist") == None:
      return """Well one of us messed up but IDK who.<br />
      In other words, one of "gametype", "game", or "wordlist" isn't in the URL.""",400

    gametype = flask.request.args["gametype"]
    if gametype == "0": # multiple choice
      return flask.render_template(
        "multi-choice.html"
      )
    else:
      return "Game type not implemented."
  elif flask.request.method == "POST":
    user = flask_login.current_user
    game = str(int(flask.request.form["game"]))
    gametype = str(int(flask.request.form["gametype"]))
    wordlist = str(int(flask.request.form["wordlist"]))
    score = int(flask.request.form["score"])
    
    scores = UserDB.query.get(user.id).student.scores
    if (game+","+gametype+","+wordlist) in scores:
      scores[game+","+gametype+","+wordlist] = score if score > scores[game+","+gametype+","+wordlist] else scores[game+","+gametype+","+wordlist]
    else:
      scores[game+","+gametype+","+wordlist] = score
    UserDB.query.get(user.id).student.scores = scores
    db.session.commit()
    return "Updated"
  else:
    return "This should never happen.",400

@app.route("/game-select",methods=["GET", "POST"])
def game_select():
  form = GameSelectForm()
  if flask.request.method == "GET":
    return flask.render_template(
      "game-select.html",form=form
    )
  if flask.request.method == "POST":
    if form.validate() == False:
      flask.flash("All fields are required")
      return flask.render_template("game-select.html",form=form)
    return flask.redirect(flask.url_for(
      "game",
      gametype=flask.request.form["game_type"],
      game=flask.request.form["game"],
      wordlist=flask.request.form["wordlist"]
    ))
  return "This also shouldn't happen.",400

@app.route("/scores", methods=["GET"])
@flask_login.login_required
def scores():
  user = flask_login.current_user
  if user.isTeacher:
    return "Not available to teachers"
  dbscores = UserDB.query.get(user.get_id()).student.scores
  scores = [[gameConvert([int(x) for x in score.split(",")]),dbscores[score]] for score in dbscores.keys()]
  return flask.render_template("scores.html", scores=scores)

@app.route("/classes", methods=["GET"])
@flask_login.login_required
def classes():
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go back to your homework!!",401
  
  classes = UserDB.query.get(flask_login.current_user.id).teacher.studentclasses
  classes = [cl.name for cl in classes]
  return flask.render_template("classes/classes.html",classes=classes)

@app.route("/class/<classname>")
@flask_login.login_required
def class_info(classname):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go back to your speedtests!!",401

  classes = UserDB.query.get(flask_login.current_user.id).teacher.studentclasses
  class_not_the_keyword = [cl for cl in classes if cl.name == classname][0]
  if class_not_the_keyword == None:
    return flask.render_template("classes/class_not_found.html")
  else:
    assignments = [gameConvert(assignment) for assignment in class_not_the_keyword.assignments]
    return flask.render_template("classes/class_info.html",cl=class_not_the_keyword, assignments=assignments)

@app.route("/class/<classname>/student_scores", methods=["GET"])
@flask_login.login_required
def student_scores(classname):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go back to your revision!!",401
  # maybe return HTTP 102 or 202
  class_not_the_keyword = StudentClassDB.query.get(getClass(UserDB.query.get(flask_login.current_user.id).teacher,classname))
  scores = [[gameConvert(assignment),[getScore(student.scores,assignment) for student in class_not_the_keyword.students]] for assignment in class_not_the_keyword.assignments]
  students = [student.user.username for student in class_not_the_keyword.students]
  # equivalent of the list comprehention for non-psychopaths.
  #for assignment in class_not_the_keyword.assignments:
  #  scores.append(gameConvert(assignment))
  #  for student in class_not_the_keyword.students:
  #    scores[-1].append(get_score(student.scores,assignment))
  # ends with [converted assignment, student score for every student]

  return flask.render_template("classes/student_scores.html", scores=scores, students=students, classname=classname)

@app.route("/class/<classname>/create_student", methods=["GET","POST"])
@flask_login.login_required
def add_student(classname): # create a new UserDB and StudentDB and add StudentDB to class
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go back to your <a href='https://youtu.be/k1Zu0F6gUn8'>Altai throat singing</a>!!",401
  form = SignupForm()
  if flask.request.method == "POST":
    username = flask.request.form["username"]
    email = flask.request.form["email"]
    password = flask.request.form["password"]
    # TODO: verify email
    if form.validate() == False:
      flask.flash("All fields are required")
      return flask.render_template("classes/add_student.html",classname=classname,form=form)
    else:
      if UserDB.query.filter_by(username=username).first() == None and UserDB.query.filter_by(email=email).first() == None:
        new_user = UserDB(username=username, password_hash=hashing.hash_value(password), email=email, isTeacher=False)
        new_student = StudentDB(user=new_user)
        db.session.add(new_user)
        db.session.add(new_student)
        class_not_the_keyword = [cl for cl in UserDB.query.get(flask_login.current_user.id).teacher.studentclasses if cl.name == classname][0]
        class_not_the_keyword.students.append(new_student)
        db.session.commit()
        return flask.redirect(flask.url_for("class_info",classname=classname))
    flask.flash("Username or email already in use.")
    return flask.render_template('classes/add_student.html',classname=classname, form=form)
  if flask.request.method == "GET":
    return flask.render_template("classes/add_student.html",classname=classname,form=form)
  return "OADOIWANFOCIO WHATONEARTHHASHAPPENED HERE OWIJAOIFOINOAWVDIAWOVENOOMICANOSJKAWFKN"

@app.route("/add_class", methods=["GET", "POST"])
@flask_login.login_required
def add_class():
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go back to your <a href='https://redd.it'>Reddit</a>!!",401
  if flask.request.method == "GET":
    return flask.render_template("classes/add_class.html")
  if flask.request.method == "POST":
    new_class = StudentClassDB(name=flask.request.form["name"], teachers=[UserDB.query.get(flask_login.current_user.id).teacher])
    db.session.add(new_class)
    db.session.commit()
    return flask.redirect(flask.url_for("classes"))
  return "agaeafeaefnioeainofwnoiewfnoiaewfoin"

@app.route("/class/<classname>/add_student", methods=["GET","POST"])
@flask_login.login_required
def add_student_to_class(classname):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go and watch <a href='https://youtu.be/PP0nxriOVio'>https://youtu.be/PP0nxriOVio</a> or somethin !!",401
  classes = UserDB.query.get(flask_login.current_user.id).teacher.studentclasses
  class_not_the_keyword = [cl for cl in classes if cl.name == classname][0]
  if flask.request.method == "GET":
    students = []
    for cl in classes:
      for student in cl.students:
        if student not in class_not_the_keyword.students and student not in students:
          students.append(student)
    return flask.render_template("classes/add_student_to_class.html", students=students)
  if flask.request.method == "POST":
    students_in_teachers_classes = []
    for cl in classes:
      for student in cl.students:
        if student not in students_in_teachers_classes:
          students_in_teachers_classes.append(student)
    
    if StudentDB.query.get(int(flask.request.form["student_id"])) not in students_in_teachers_classes:
      return "Student not in one of your classes."

    students = class_not_the_keyword.students
    students.append(StudentDB.query.get(int(flask.request.form["student_id"])))
    class_not_the_keyword.students = students
    db.session.commit()
    return flask.redirect(flask.url_for("class_info",classname=class_not_the_keyword.name))
  return "nasdvjskjfapwjkfjvdncsao"

@app.route("/class/<classname>/delete", methods=["GET"])
@flask_login.login_required
def delete_class(classname):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go and watch <a href='https://netflix.com'>Netflix</a> or somethin !!",401
  return flask.render_template("classes/delete_class_confirm.html", classname=classname)

@app.route("/class/<classname>/delete/confirm", methods=["GET"])
@flask_login.login_required
def delete_class_confirmed(classname):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go and watch <a href='https://disneyplus.com'>Disney plus</a> or somethin !!",401
  class_not_the_keyword = [cl for cl in UserDB.query.get(flask_login.current_user.id).teacher.studentclasses if cl.name == classname][0]
  if class_not_the_keyword == None:
    return flask.redirect(flask.url_for("classes"))
  db.session.delete(class_not_the_keyword)
  db.session.commit()
  return flask.redirect(flask.url_for("classes"))

@app.route("/class/<classname>/remove_student", methods=["GET","POST"])
@flask_login.login_required
def remove_student_from_class(classname):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go touch grass !!",401

  class_not_the_keyword = getClass(UserDB.query.get(flask_login.current_user.id).teacher)
  if flask.request.method == "GET":
    return flask.render_template("classes/remove_student.html", students=StudentClassDB.query.get(class_not_the_keyword).students, classname=classname)
  if flask.request.method == "POST":
    students = StudentClassDB.query.get(class_not_the_keyword).students
    try:
      students.remove(StudentDB.query.get(int(flask.request.form["student_id"])))
    except ValueError:
      return "afnawdnawkjfkjawnwaonfwonaon"
    StudentClassDB.query.get(class_not_the_keyword).students = students
    db.session.commit()
    return flask.redirect(flask.url_for("class_info", classname=classname))

@app.route("/class/<classname>/set_assignment", methods=["GET", "POST"])
@flask_login.login_required
def set_assignment(classname):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go play Mario Kart!!",401
  form = GameSelectForm()
  if flask.request.method == "GET":
    return flask.render_template("classes/set_assignment.html", classname=classname, form=form)
  if flask.request.method == "POST":
    if form.validate() == False:
      flask.flash("All fields are required")
      return flask.render_template("classes/set_assignment.html", classname=classname, form=form)

    class_not_the_keyword = getClass(UserDB.query.get(flask_login.current_user.id).teacher,classname)
    assignments = StudentClassDB.query.get(class_not_the_keyword).assignments
    assignment = [int(flask.request.form["game"]),int(flask.request.form["game_type"]),int(flask.request.form["wordlist"])]
    if assignment not in assignments:
      assignments.append(assignment)
      StudentClassDB.query.get(class_not_the_keyword).assignments = assignments
      db.session.commit()
      flask.flash("Assignment set succesfully")
      return flask.render_template("classes/set_assignment.html", classname=classname, form=form)

    flask.flash("Assignment set failed. Probably already set.")
    return flask.render_template("classes/set_assignment.html", classname=classname, form=form)

@app.route("/class/<classname>/assignment/<assignment_id>/cancel", methods=["GET"])
@flask_login.login_required
def cancel_assignment(classname, assignment_id):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go eat crisps or something!!",401
  
  class_not_the_keyword = StudentClassDB.query.get(getClass(UserDB.query.get(flask_login.current_user.id).teacher,classname))
  assignment = gameConvert(class_not_the_keyword.assignments[int(assignment_id)])
  
  return flask.render_template("classes/cancel_assignment_confirm.html", classname=classname, assignment=assignment, assignment_id=assignment_id)

@app.route("/class/<classname>/assignment/<assignment_id>/cancel/confirm", methods=["GET"])
@flask_login.login_required
def cancel_assignment_confirm(classname, assignment_id):
  if not flask_login.current_user.isTeacher:
    return "Teachers only! Go eat biscuits or something!!",401

  class_not_the_keyword = getClass(UserDB.query.get(flask_login.current_user.id).teacher,classname)
  assignments = StudentClassDB.query.get(class_not_the_keyword).assignments
  del assignments[int(assignment_id)]
  StudentClassDB.query.get(class_not_the_keyword).assignments = assignments
  db.session.commit()
  return flask.redirect(flask.url_for("class_info", classname=classname))

@app.route("/assignments", methods=["GET"])
@flask_login.login_required
def assignments():
  if flask_login.current_user.isTeacher:
    return "This is for students! Make a cup of coffee or something!!",401
  
  dbscores = UserDB.query.get(flask_login.current_user.id).student.scores
  assignments = UserDB.query.get(flask_login.current_user.id).student.studentclass.assignments
  assignments_string = [
    gameConvert(assignment) + list(str(getScore(dbscores,assignment)))
    for assignment in assignments
  ]
  return flask.render_template("assignments.html", assignments=assignments_string, assignment_ids=assignments)

@app.route("/settings", methods=["GET"])
@flask_login.login_required
def account_settings():
  if flask_login.current_user.isTeacher:
    return flask.render_template("accounts/settings-teacher.html", user=UserDB.query.get(flask_login.current_user.id))
  else:
    return flask.render_template("accounts/settings-student.html", user=UserDB.query.get(flask_login.current_user.id))

@app.route("/settings/change-password", methods=["GET","POST"])
@flask_login.login_required
def change_password():
  if flask.request.method == "GET":
      return flask.render_template("accounts/change-password.html", failed=False)
  if flask.request.method == "POST":
    user = UserDB.query.get(flask_login.current_user.id)
    if flask.request.form["new"] == flask.request.form["confirm"] and hashing.hash_value(flask.request.form["current"]) == user.password_hash:
      user.password_hash = hashing.hash_value(flask.request.form["new"])
      db.session.commit()
      return flask.redirect(flask.url_for("index"))
    else:
      return flask.render_template("accounts/change-password.html", failed=True)
  return "Oops, I did something stupid. Dunno what. Please tell me."

@app.route("/settings/change-username", methods=["GET","POST"])
@flask_login.login_required
def change_username():
  return "Not done yet. Sry. Contact me directly if you need this changing."

@app.route("/settings/change-email", methods=["GET","POST"])
@flask_login.login_required
def change_email():
  return "Not done yet. Sry. Contact me directly if you really need this replacing."

@app.route("/settings/delete-account", methods=["GET","POST"])
@flask_login.login_required
def delete_account():
  return "Not done this yet, sorry. Contact me directly to delete your account"
